const path = require('path');
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
    entry: {
        bundle: './src/js/index.js',
    },
    output: {
        path: path.join(__dirname, "dist"),
        filename: "main.js"
    },
    module: {
        rules: [
          {
            test: /\.css$/i,
            use: ['style-loader', 'css-loader'],
          },
        ],
    },
    mode: 'development',
    plugins: [
        new CopyWebpackPlugin([
            {
                from: "src/index.html",
                to: "index.html"
            },
            {
                from: "src/css",
                to: "./css/"
            }
        ]),
    ]

};
