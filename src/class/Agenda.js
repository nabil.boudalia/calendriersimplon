import Evenement from './Evenement';
import moment from "moment";

const STORAGE_NAME = 'agenda';

export default class Agenda {

    constructor() {

        this.events = [];
        const storage = localStorage.getItem(STORAGE_NAME);
        
        storage == null ? [] : this.initFromStorage(storage);
    }

    ajouteEvenement(name, date) {

        this.events.push(

            new Evenement(name, moment(date))
        );
        this.setStorage();
    }

    supprimeEvenement(i) {

        this.events.splice(i, 1);
        this.setStorage();
    }

    initFromStorage(storage) {

        const arrayDate = JSON.parse(storage);

        for(var i in arrayDate) {

            this.events.push(
                new Evenement(arrayDate[i].name, moment(arrayDate[i].date))
            )
        }
    }

    setStorage() {

        localStorage.setItem(STORAGE_NAME, JSON.stringify(this.events));
    }

    getAllEvents() {

        const currentDay = moment();

        return this.events.map(e => {

            const date = e.getDate().set('year', currentDay.year());

            const diff = date.diff(currentDay,'days');

            const tmpAnniv = date.set('year', currentDay.year() + 1);

            return {
                name: e.getName(),
                date: e.getDate().set('year', currentDay.year()),
                counter: diff < 0 ? tmpAnniv.diff(currentDay,'days') : diff
            }
        });
    }

    getEvenementsToday() {

        let res = [];

        for(var i in this.events) {

            if(this.events[i].isToday()) {

                res.push(this.events[i].getName());
            }
        }

        return res;
    }

}