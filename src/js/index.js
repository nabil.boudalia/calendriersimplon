import Agenda from '../class/Agenda';
import moment from "moment";
import 'jquery-ui/themes/base/core.css';

import { Calendar } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';


import '@fullcalendar/core/main.css';
import '@fullcalendar/daygrid/main.css';


document.addEventListener('DOMContentLoaded', function() {
    

    const myAgenda = new Agenda();

    //myAgenda.ajouteEvenement("nom test", "2019-09-25"); //add specific event 

    var calendarEl = document.getElementById('calendar');

    var calendar = new Calendar(calendarEl, {
        plugins: [ dayGridPlugin, interactionPlugin ],
        dateClick: addEvent,
        eventClick: eventClick
    });

    calendar.render();

    function eventClick(info) {

        if(confirm("Etes-vous sur de vouloir supprimer cet evenement ?")) {

            myAgenda.supprimeEvenement(info.event.id);
            displayEvents(myAgenda.getAllEvents());
        }
    }


    function addEvent(info) {

        const date = info.dateStr;
        const eventName = prompt("Nom de l'événement");
        myAgenda.ajouteEvenement(eventName, date);
        displayEvents(myAgenda.getAllEvents());
    }

    displayEvents(myAgenda.getAllEvents());

    function displayEvents(listEvents) {

        calendar.removeAllEvents();

        for(var i in listEvents) {

            calendar.batchRendering(function() {
    
                calendar.changeView('dayGridMonth');
                calendar.addEvent({ title: listEvents[i].name, start: listEvents[i].date.format("YYYY-MM-DD"), id: i});
            });
        }
    }

    function displayCurrentDate() {

        document.getElementById("dateJour").innerHTML = moment().format('DD MMMM HH:mm');
    }
    
    displayCurrentDate()
    setInterval(() => {

        displayCurrentDate();
    }, 1000);

    
});

